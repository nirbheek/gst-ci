FROM registry.fedoraproject.org/fedora:29

# System setup
RUN echo "fastestmirror=true" >>  /etc/dnf/dnf.conf && \
    dnf upgrade -y && \
    dnf install -y sudo git rsync

# Configure git for various usage
RUN git config --global user.email "cerbero@gstreamer.freedesktop.org" && \
    git config --global user.name  "Cerbero Build System"

RUN git clone https://gitlab.freedesktop.org/gstreamer/cerbero.git && \
    mkdir $HOME/.cerbero && \
    echo "allow_parallel_build=True" > $HOME/.cerbero/cerbero.cbc && \
    echo "use_ccache=True" >> $HOME/.cerbero/cerbero.cbc && \
    cd cerbero && \
      echo "local_sources=\"/cerbero/cerbero-sources\"" >> localconf.cbc && \
      echo "home_dir=\"/cerbero/cerbero-build\"" >> localconf.cbc && \
      ./cerbero-uninstalled -c localconf.cbc bootstrap -y --system-only && \
      ./cerbero-uninstalled -c localconf.cbc fetch-bootstrap && \
      ./cerbero-uninstalled -c localconf.cbc fetch-package gstreamer-1.0 && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win32.cbc fetch-bootstrap && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win32.cbc fetch-package gstreamer-1.0 && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win32.cbc bootstrap -y --system-only && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win64.cbc fetch-bootstrap && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win64.cbc fetch-package gstreamer-1.0 && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-win64.cbc bootstrap -y --system-only && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-android-universal.cbc fetch-bootstrap && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-android-universal.cbc fetch-package gstreamer-1.0 && \
      ./cerbero-uninstalled -c localconf.cbc -c config/cross-android-universal.cbc bootstrap -y --system-only && \
      cd .. && \
    rm /cerbero/cerbero-sources/android-ndk-*.zip && \
    rm /cerbero/cerbero-sources/mingw-w*.tar.xz && \
    rm -rf /cerbero/cerbero-build/{dist,logs,sources} && \
    rm -f /cerbero/cerbero-build/{linux,windows,android}*.cache && \
    dnf clean all
